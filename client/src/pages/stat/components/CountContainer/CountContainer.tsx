import { Image, Text, View } from '@tarojs/components'
import { Config, useState } from '@tarojs/taro'
import './index.less'
import StatBG from '../../assets/统计-bg.png'

export function CounterContainer() {
  return (
    <View className="count-container">
      <View className="count">
        <Text className="num">299</Text>
        <Text className="tip">健康</Text>
      </View>
      <View className="line"></View>
      <View className="count" style={{ flex: 1 }}>
        <Text className="num">2</Text>
        <Text className="tip">有发热</Text>
      </View>
      <View className="line"></View>
      <View className="count">
        <Text className="num">20</Text>
        <Text className="tip">其他症状</Text>
      </View>
    </View>
  )
}
